# Procédure d'utilisation de jsDOC
> JSDoc 3 is an API documentation generator for JavaScript,
> 
> similar to Javadoc or phpDocumentor. You add documentation
> 
> comments directly to your source code, right alongside the
>  
> code itself. The JSDoc tool will scan your source code and 
> 
> generate an HTML documentation website for you.
>
## Node js

vérifiez que nodejs est installé :

https://nodejs.org/en/

Dans la console code :

    node --version

Si aucune version intallées allez sur le site et téléchargez la version LTS (long term support)


## NPM
https://www.npmjs.com/
nodejs intalle par défaut npm

vérifier que NPM est à jour :

dans la console code :

    npm --version

si besoinde mettre a jour  : 

    npm update -g npm


# jsDOC

https://jsdoc.app/



## installation global de jsdoc :
***(disponible pour tout vos projet)***

    npm install -g jsdoc


## Commenter son code

Pour pouvoir générer la documenttation vos Class, modules et fonction doivent etre commenter
avec la syntaxe JSDoc :

https://fr.wikipedia.org/wiki/JSDoc

https://www.editions-eni.fr/open/mediabook.aspx?idR=58cbd01f27a3f2f8b5f4b8f43281724b

Les balises JSDoc De Commentaires sont  :

```javascript
    /** 
    *
    */
```

### exemple :
```javascript
/**
 * Exemple de commentaire format jsDOC
 */

function exemples() {

}
```

Il est possible de commenter et typer des argumments

```javascript
/**
 * 
 * @param {number} arg1 - description de l'argument 1
 * @param {string} arg2 - description de l'argument 2
 */
function exemplestype(arg1, arg2) {


}
```
il est possible d'utiliser des extension VsCode pour commenter son code : 

Mintlify Doc Writer - génération automatique par AI de la doc

https://marketplace.visualstudio.com/items?itemName=mintlify.document

Add jsdoc comments 

https://marketplace.visualstudio.com/items?itemName=stevencl.addDocComments

Document This 

https://marketplace.visualstudio.com/items?itemName=oouo-diogo-perdigao.docthis

Et bien d'autres.

# Générer la doc

## Un seul fichier

Pour utiliser jsDOC

dans le terminal tapez :

    jsdoc et le nom de votre fichier js
    dans notre cas :

    jsdoc exemples.js

Cela vas creer un dossier /out par défaut.
Dans le dossier seront créé dossiers qui sont les dossiers de mise en forme de notre documentation.

Et seront créé des fichiers HTML
- index.html qui est la page d'accueil de notre documentation
- global.html qui la page ou toute nos fonctions vont etre listées
- 'nomdufichier'.js.html notre code contenu dans le fichier js
  
<figure>
    <img src="img/index.html.png"
         alt="index.html"
         width ="900" height="600">
    <figcaption>index.html</figcaption>
</figure>

<figure>
    <img src="img/global.png"
         alt="global.html"
         width ="900" height="600">
    <figcaption>global.html</figcaption>
</figure>

<figure>
    <img src="img/exemples.js.html.png"
         alt="exemples.js.html"
         width ="900" height="600">
    <figcaption>exemples.js.html</figcaption>
</figure>

## un dossier

Pour générer la doc de tout les fichiers d'un dossier :

    jsdoc chemin du dossier
    dans notre exemple :

    jsdoc ./js
## plusieurs fichiers ou dossier ou des fichiers et des dossiers

    jsdoc exemples.js ./js README.md

## Fichier de configuration

il est possible d'éditer un fichier de configuration pour JSDOC 

https://jsdoc.app/about-configuring-jsdoc.html

cela permet de spécifier les dossiers/fichier a exclure ou iclure dans la génération jsdoc

## Readme

Il est possible d'alimenter la page index avec un fichier README.md
avec la commande : 

    jsdoc README.md

# HTML to DOC

## Fonction Bonus

Dans le fichier exemple vous trouverez la fonction Export2Doc() qui permert d'exporter votre page au format word

Il vous suffit de rajouter un bouton dans votre page HTML

```HTML
<button onclick='Export2Doc(element,filename)' value="bouton"></button>
```
- element - L'élément à exporter.
- filename - Le nom du fichier à télécharger.

Par exemple sur la page global.html il y a une div avec une id="main" qui prend tout le contenue de la page.

Par exemple : 
```HTML
<button onclick='Export2Doc("main","mon fichier source")' value="bouton"></button>
```
téléchargera une fichier doc avec le contenue de la page global.html

<figure>
    <img src="img/global%20word.png"
         alt="global en format word"
         width ="900" height="600">
    <figcaption>global en format word</figcaption>
</figure>

